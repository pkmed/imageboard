bodyOnload();

/**
 * Initialize some things on body load
 */
async function bodyOnload() {
    if (location.pathname.match(/^\/[a-z]{1,4}\/res\/[0-9]*/)) {
        await loadPosts(500);
        let lastSeen = location.href.split('#')[1];
        if (lastSeen && lastSeen.length > 0) {
            highlightReferenced(lastSeen);
        }
    }
    anchorLinkUpFocusHandler();
    callInitializers();
    setListeners();
}

/**
 * Calls initializing functions
 * @return {Promise<void>}
 */
async function callInitializers() {
    upDownBtns();
    initFavourites();
}

/**
 * Sets listeners to some of elements
 */
function setListeners() {
    $("body").on("wheel", () => {
        upDownBtns();
    });
    let floatingForm = $("#floatingForm");
    if (floatingForm != null) {
        dragElement(floatingForm);
    }
    for (const form of $(".editForm")) {
        dragElement($(form));
    }
    for (const btn of $(".sendPostBtn")) {
        btn.onclick = sendPost;
    }
    initWindowScrollListener();
    initCtrlEnterPostSending('upperForm');
    initCtrlEnterPostSending('lowerForm');
    initCtrlEnterPostSending('floatingForm');
}

/**
 * Switches NSFW mark
 * NSFW - not suitable for work
 */
function setNSFW() {
    let value = localStorage.getItem("NSFW") === "true";
    localStorage.setItem("NSFW", (!value).toString());

    let attachs = $('.post-attachment');
    for (attachment of attachs) {
        checkNSFW(attachment);
    }
}

/**
 * Inserts text formatting tags
 *
 * @param tagS {string} Start tag
 * @param tagE {string} Closing tag
 * @param btn {HTMLElement} Btn with tag
 */
function addTags(tagS, tagE, btn) {
    let textArea = $(btn).parent().prev().children().last()[0];
    if (window.getSelection().toString() !== "") {
        let start = textArea.selectionStart;
        let end = textArea.selectionEnd;
        let selectedText = textArea.value.substr(start, end - start);
        textArea.setRangeText(tagS + selectedText + tagE, start, end, "start");
        textArea.selectionStart += tagS.length;
        textArea.setSelectionRange(textArea.selectionStart, textArea.selectionStart + selectedText.length);
    } else {
        textArea.setRangeText(tagS + tagE, textArea.selectionStart, textArea.selectionEnd, "preserve");
        textArea.selectionStart += tagS.length;
    }
    textArea.focus();

}

/**
 * Loads posts on reaching bottom of page, is there's not all posts on page
 * @param amount {Number} Posts amount to load
 */
async function loadPosts(amount = null) {
    let postList = $("#postList");
    let postsNumber = $(".postsNumber")[0].innerText;
    let link;

    if (postList.children().length < postsNumber) {
        if (!amount) {
            link = loadPostsUri(postList.children().length);
        } else {
            link = loadPostsUri(postList.children().length) + "?amount=" + (amount - 15);
        }
        let response = await executeFetch(link);
        processFetchResponse(response, null, null, () => {
            for (const elem of response.posts) {
                postList.append(elem);
            }
            upDownBtns();
        });
        return true;
    }
    return false;
}

/**
 * Load full version of attachment and displays it
 *
 * @param elem {HTMLElement} Post
 * @param path {string} Path for ajax-query
 */
async function showAttachment(elem, path) {
    let result = await executeFetch(path);
    processFetchResponse(result, null, null, () => {
        let attachmentContainer = createAttachmentContainer(result);
        let attachment = createAttachment(result, attachmentContainer);
        let closeBtn = createCloseBtn(attachmentContainer);
        attachmentContainer.append(attachment);
        attachmentContainer.append(closeBtn);
        $(elem).after(attachmentContainer);
        dragElement(attachmentContainer);
    });
}

/**
 * Shows mentioned post preview
 *
 * @param elem {String} Parent post id
 * @param path {string} ajax-query
 */
async function showPreview(elem, path) {
    let reference;
    const previewData = await executeFetch(path);
    processFetchResponse(previewData, () => {
        reference = createPostNotFound();
        let link = $("#" + elem);
        link.after(reference);
        setListenersToPreview(reference);
    }, null, () => {
        if ($("#" + previewData.post_id)[0] == null) {
            $("body").before(previewData.post);
            reference = $("#" + previewData.post_id);
            let link = $("#" + elem);
            link.after(reference);
            setListenersToPreview(reference);
            let parentPost = $("#" + elem.split("_")[1]);
            calculateCoordinates(parentPost, reference);
        }
    });
}

/**
 * Calculates coordinates if preview shows out of window border
 * @param elem      {jQuery} Post from which preview was called
 * @param reference {jQuery} Referenced post
 */
function calculateCoordinates(elem, reference) {
    let lowerEdgePos = elem[0].getBoundingClientRect().y + elem[0].clientHeight + reference[0].clientHeight;
    let rightEdgePos = elem[0].getBoundingClientRect().x + elem[0].clientWidth + reference[0].clientWidth;
    if (lowerEdgePos > window.innerHeight &&
        window.innerHeight - reference[0].getBoundingClientRect().y < reference[0].clientHeight
        || rightEdgePos > window.innerWidth) {
        reference.css("top", -elem[0].getBoundingClientRect().y * 0.9 + "px");
        reference.css("left", "25px");
    }
}

/**
 * Handler for focus event
 */
function anchorLinkUpFocusHandler() {
    if (document.activeElement.innerHTML === "Up") {
        loadPosts();
    }
}

/**
 * Adds event listener for disappearing post
 * @param reference {jQuery} Necessary post
 */
function setListenersToPreview(reference) {
    reference.on("mouseleave", () => {
        reference.data = "mouseLeft";
    });
    reference.on("mouseenter", () => {
        reference.data = "mouseEntered";
    });
    setInterval(() => {
        if (reference.data === "mouseLeft" || reference.data !== "mouseEntered") {
            setTimeout(() => {
                if (reference.data !== "mouseEntered") {
                    reference.remove();
                }
            }, 5000);
        }
    }, 50);
}

/**
 * Listener for posts loading on reaching bottom of page
 */
function initWindowScrollListener() {
    let currentBoard = location.pathname.split('/')[1];
    let currentThread = location.pathname.split('/')[3];
    let loadLocked = false; // to avoid multiple loadings of same chunk of posts
    $(window).on("scroll", async () => {
        if ($(window).height() + $(window).scrollTop() >= $(document).height() - 100 && !loadLocked) {
            loadLocked = true;
            let anchorLinkUp = $("#up");
            anchorLinkUp.unbind("focus", anchorLinkUpFocusHandler);//to prevent double posts loading
            let isPostsLoaded = await loadPosts();
            loadLocked = false;
            currentThread = favourites.getFavourite(currentBoard, parseInt(currentThread));
            if (!isPostsLoaded && currentThread && currentThread.unseenPosts > 0) {
                updateLastSeen()
            }
            anchorLinkUp.bind("focus", anchorLinkUpFocusHandler);
        }
    });
    // if ($(document).height() === $(window).scrollTop() + $(window).height()
    //     && favourites.getFavourite(currentBoard, parseInt(currentThread))
    //     && favourites.getFavourite(currentBoard, parseInt(currentThread)).unseenPosts > 0) {
    //     updateLastSeen();
    // }
}

/**
 * Allows send post on 'ctrl+enter' combination
 *
 * @param formId {string} Post form id
 */
function initCtrlEnterPostSending(formId) {
    let form = $("#" + formId);
    if (form) {
        let postTextField = form.find("textarea#post_text");
        postTextField.bind("keypress", ev => {
            if (ev.ctrlKey && ev.key === "Enter" && document.activeElement === postTextField[0]) {
                form.find('.sendPostBtn').trigger("click");
            }
        });
    }
}

/**
 * Sends post via fetch
 *
 * @return {Promise<void>}
 */
async function sendPost() {
    let form = $(this).parents('form');
    let data = prepareFormData(form);
    let result = await executeFetch(location.href, {
        method: 'POST',
        body: data
    });
    processFetchResponse(result, null, null, (result) => {
        if (result.formErrors) {
            let alert = $(`<div class="alert alert-danger" id="errorPopUp"
                            onclick="this.style.visibility='hidden'">
                               <br/>
                               <small>Click to hide</small>
                           </div>`);
            $('body').append(alert);
            for (let error of result.formErrors) {
                $("#errorPopUp").prepend(`<li>${error}</li>`);
            }
        } else {
            if ($("#postList").children().length == $(".postsNumber")[0].innerText) {
                $("#postList").append(result.post);
                $(".postsNumber")[0].innerText = parseInt($(".postsNumber")[0].innerText) + 1;
                $(".postsNumber")[1].innerText = parseInt($(".postsNumber")[1].innerText) + 1;
            }
            form[0].reset();
            let children = form.find("#post_attachments");
            filesCounter--;
            for (let i = 1; i < children.length; i++) {
                children[i].remove();
                filesCounter--;
            }
            showHideForm(this, false, form.attr('id'));
        }
        $(".form-wrap-loader").remove();
    });
}

/**
 * Removes extra file inputs from form
 *
 * @param form {jQuery}
 * @return {FormData} properly processed data
 */
function prepareFormData(form) {
    form.append($("<div class='form-wrap-loader'></div>").append($("<div class='loader'></div>")));
    let data = new FormData(form[0]);

    let files = data.getAll("post[attachments][]");

    files = files.filter((value) => {
        return value.name !== '';
    });

    data.delete("post[attachments][]");

    for (let file of files) {
        data.append("post[attachments][]", file);
    }

    return data;
}