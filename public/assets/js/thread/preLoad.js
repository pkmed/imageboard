let show = (event) => {
    event.target.style.opacity = '1';
};

let hide = (event) => {
    event.target.style.opacity = '0.05';
};
/**
 * Provides opacity switching
 *
 * @param attachment {HTMLImageElement} Attachment thumbnail in post
 */
function checkNSFW(attachment) {
    //NSFW - not suitable for work
    if (localStorage.getItem('NSFW') === 'true') {
        attachment.style.opacity = '0.05';
        attachment.addEventListener('mouseover', show);
        attachment.addEventListener('mouseout', hide);
    } else {
        attachment.removeEventListener('mouseover', show);
        attachment.removeEventListener('mouseout', hide);
        attachment.style.opacity = '1';
    }
}