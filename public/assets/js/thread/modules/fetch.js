/**
 * @return {string} Host name
 */
function getBaseUri() {
    return location.origin;
}

/**
 * @param alias {string} Board alias
 * @param opPostId {number} Thread op-post id
 * @return {string} Ajax-query link for loading post data
 */
function loadOpPostUri(alias, opPostId) {
    return getBaseUri() + `/post/${alias}/${opPostId}`;
}

function threadUri(alias, opPostId){
    return getBaseUri() + `/${alias}/res/${opPostId}`;
}

/**
 * @param number {number} Number of displayable reports
 * @return {string} Ajax-query link for loading reports
 */
function loadReportsUri(number) {
    return getBaseUri() + `/report/load?reportsNumber=${number}`;
}

/**
 * @param offset {number} Last displayed post
 * @return {string} Ajax-query link for loading posts
 */
function loadPostsUri(offset) {
    return getBaseUri() + location.pathname + "/" + offset;
}

/**
 * @param alias {string} Board alias
 * @param lastSeenPost {number} Post id
 * @return {string} Ajax-query link for unseen posts count
 */
function getUnseenPostsUri(alias, lastSeenPost) {
    return getBaseUri() + `/unseen_posts/${alias}/${lastSeenPost}/`
}

/**
 * Executes ajax request for given path
 *
 * @param path {string} Path for ajax query
 * @param params {Object} Path for ajax query
 * @return {Promise <JSON|boolean|number>} JSON on 200, false on 404, error code otherwise
 */
async function executeFetch(path, params={}) {
    const response = await fetch(path, params);
    if (response.status === 200) {
        return response.json();
    } else if (response.status === 404) {
        return false;
    } else {
        return response.status;
    }
}

/**
 * Ajax response processing
 *
 * @see executeFetch
 * @param response {JSON|number|boolean} Response from executeAjax()
 * @param on404Handler {Function|null} Handler for error 404
 * @param onOtherErrorHandler {Function|null} Handler for other error codes
 * @param onSuccessHandler {Function|null} Handler for response-code 200
 */
function processFetchResponse(response, on404Handler = null, onOtherErrorHandler = null, onSuccessHandler) {
    if (!response) {
        if (on404Handler === null) {
            alert("Error during the process");
        } else {
            on404Handler();
        }
    } else if (typeof response === "number") {
        if (onOtherErrorHandler === null) {
            console.log(`Error: ${response}`);
        } else {
            onOtherErrorHandler(response);
        }
    } else {
        onSuccessHandler(response);
    }
}