loadReports();

/**
 * Displays window for sending report
 *
 * @param {string} path For ajax request
 * @see executeFetch
 * @see processFetchResponse
 */
async function sendReport(path) {
    let reason = prompt("Describe reason:");
    while (reason === "") {
        alert("Empty reason not allowed!");
        reason = prompt("Describe reason:");
    }
    if (reason != null) {
        let result = await executeFetch(path + "?reason=" + reason);
        processFetchResponse(result, null, null, () => {
            alert(result);
        });
    }
}

/**
 * Report deleting
 *
 * @param {HTMLButtonElement} btn Button in a row
 * @param {string} path For ajax request
 * @see executeFetch
 * @see processFetchResponse
 */
async function deleteReport(btn, path) {
    let result = await executeFetch(path);
    processFetchResponse(result, null, null, () => {
        let reportList = $("#reportListUl");
        if (reportList.children().length - 1 === 0) {
            reportList.children()[0] = $("<li></li>");
            reportList.children()[0].text("No reports were left");
        } else {
            $(btn).parent().remove();
        }
        alert(result);
        if (btn.className.includes("Post")) {
            location.reload();
        }
    });
}

/**
 * Loading undisplayed reports
 * @see executeFetch
 * @see processFetchResponse
 * @see updateBtnRotation
 */
async function loadReports(btn) {
    let rotateInterval;
    if (btn) {
        rotateInterval = updateBtnRotation(btn);
    }
    let reportList = $("#reportListUl");
    let reportRows = $(".reportRow");
    let reports = await executeFetch(loadReportsUri(reportRows.length));
    processFetchResponse(reports, null, () => {
    }, () => {
        if (reports.length > 0) {
            if (reportList.children()[0].textContent === "No reports were left") {
                reportList.children()[0].remove();
            }
            //reports contains rendered html in this case
            reportList.append($(reports.toString()));
        }
    });
    if (btn) {
        clearInterval(rotateInterval);
        btn.style.transform = "rotate(0deg)";
    }
}