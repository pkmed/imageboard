/**
 * Animation for displaying and hiding upper or lower form
 *
 * @param triggerBtn {HTMLElement} Trigger button for show/hide
 * @param switchText {boolean} If true, switches text of <b>triggerBtn</b> like "Show form\Hide form"
 * @param formId {string} Id of form
 */
function showHideForm(triggerBtn, switchText, formId) {
    $('#'+formId).toggle();

    if(switchText){
        if($(triggerBtn).html() === "Hide form"){
            $(triggerBtn).html("Show form");
        } else {
            $(triggerBtn).html("Hide form");
        }
    }
}

/**
 * Animation for displaying floating form
 *
 * @param answerTo {string} Post id to answer to
 */
function showFloatingForm(answerTo) {
    let form = $('#floatingForm');
    form.css("display", "block");
    let input = $("#text").children().last();
    input.val(input.val() + ">>" + answerTo + "\n");
    let quote = window.getSelection().toString();
    if (quote !== "") {
        let selectedPost = $(window.getSelection().focusNode.parentElement);
        selectedPost = selectedPost.parents('.post');
        if (selectedPost.attr("id") === answerTo) {
            for (const string of quote.split("\n")) {
                if (string.substr(0, 1) !== ">") {
                    input.val(input.val() + ">" + string + "\n");
                } else {
                    input.val(input.val() + string + "\n");
                }
            }
        }
    }
    input.focus();
}

/**
 * Animation for displaying floating edit form
 *
 * @param formId {string} Form id to show
 */
function showFloatingEditForm(formId) {
    let form = $('#'+formId);
    let post = $('#'+formId.slice(8));
    form.css("display" , "block");

    let sender = post.find(".postSender");
    let theme = post.find(".postTheme");
    let text = post.find(".postText");

    form.children().eq(1).children().last().val(sender.text().trim());
    form.children().eq(2).children().last().val(theme.text().trim());
    form.children().eq(3).children().last().val(text.text().trim());
}

/**
 * Hides corresponding floating btn on scrolling page if needed
 */
function upDownBtns() {
    let downBtn = $("#btnDown");
    let upBtn = $("#btnUp");
    if (window.innerHeight + window.scrollY >= document.body.scrollHeight - 300) {
        downBtn.css("display", "none");
    } else {
        downBtn.css("display", "block");
    }
    if (window.scrollY <= 300) {
        upBtn.css("display", "none");
    } else {
        upBtn.css("display", "block");
    }
}

/**
 * Highlights certain post after clicking on reflink
 *
 * @param id {string} Post id
 */
async function highlightReferenced(id) {
    let post = $('#'+id);
    $("html, body").animate({ scrollTop: post.offset().top }, 0);
    post.css("backgroundColor", "#d67e00");
    setTimeout(() => {
        post.css("backgroundColor", "#fff");
    }, 1000);
}

/**
 * Jump to top of the page
 */
function toTop() {
    $("html, body").animate({ scrollTop: 0 }, 0);
    $("#btnDown").css("display", "block");
    $("#btnUp").css("display", "none");
}

/**
 * Jump to the bottom of the page
 */
function toBottom() {
    $("html, body").animate({ scrollTop: $(document).height() }, 0);
    $("#btnDown").css("display", "none");
    $("#btnUp").css("display", "block");
}

/**
 * Resizing loaded attachment
 *
 * @param e {WheelEvent} Wheel event
 * @param aspectRatio {number} Attachment dimensions aspect ratio
 * @param container {jQuery} Parent container of attachment
 */
function resizeAttachment(e, aspectRatio, container) {
    let sizeDelta = 50;
    if (e.deltaY < 0) { //scroll up
        container.css("height", parseInt(container.css("height"), 10) + sizeDelta + "px");
        if (aspectRatio > 1) {
            container.css("width", Math.round(parseInt(container.css("width"), 10) + sizeDelta * aspectRatio) + "px");
        }
    }
    if (e.deltaY > 0) { //scroll down
        container.css("height", parseInt(container.css("height"), 10) - sizeDelta + "px");
        if (aspectRatio > 1) {
            container.css("width", Math.round(parseInt(container.css("width"), 10) - sizeDelta * aspectRatio) + "px");
        }
    }
}

/**
 * For preventing page from scrolling on wheel-event on loaded attachment
 *
 * @param e {WheelEvent}
 */
function preventDefault(e) {
    e.preventDefault();
    e.returnValue = false;
}

/**
 * Makes elem draggable
 *
 * @param elem {jQuery}
 */
function dragElement(elem) {
    $("#"+elem.attr("id") + "Header").mousedown(e => {
        dragMouseDown(e, elem);
    });
}

/**
 * Handler for elem.onmousedown
 *
 * @param e {MouseEvent}
 * @param elem {jQuery}
 */
function dragMouseDown(e, elem) {
    e.preventDefault();
    let deltaX;
    let deltaY;
    let posX;
    let posY;
    // get the mouse cursor position at startup:
    posX = e.clientX;
    posY = e.clientY;
    $(document).on({
        mouseup: () => {
            // stop moving when mouse button is released:
            $(document).off('mouseup');
            $(document).off('mousemove');
        },
        mousemove: e => {
            e.preventDefault();
            // calculate the new cursor position:
            deltaX = posX - e.clientX;
            deltaY = posY - e.clientY;
            posX = e.clientX;
            posY = e.clientY;
            // set the element's new position:
            elem.css("top", (elem[0].offsetTop - deltaY) + "px");
            elem.css("left", (elem[0].offsetLeft - deltaX) + "px");
        },
    });
}

/**
 * Animation of rotation for update button
 *
 * @param updateBtn {HTMLElement} Certain update btn which need to rotate
 * @return {number} Interval ID to stop animation
 */
function updateBtnRotation(updateBtn) {
    let angle = 0;
    const rotation = () => {
        angle += 3;
        $(updateBtn).css("transform", `rotate(${angle}deg)`);
    };
    return setInterval(rotation, 10);
}