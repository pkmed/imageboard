let filesCounter = 0;

/**
 * Creates additional input for uploading files
 *
 * @param {HTMLInputElement} fileInput
 */
function addFileInput(fileInput) {
    if (fileInput.value !== null || fileInput.value !== "") {
        if (++filesCounter < 4) {
            let newInput = fileInput.cloneNode(false);
            newInput.value = null;
            fileInput.parentElement.appendChild(newInput);
        }
    }
}

/**
 * 'Post not found' popup
 *
 * @return {HTMLDivElement} Popup
 */
function createPostNotFound() {
    let reference = $("<div></div>");
    reference.css("position", "absolute");
    reference.css("backgroundColor", "#FFFFFF");
    reference.html("<span class=\"rounded border rounded-lg p-2\">Post not found</span>");

    return reference;
}

/**
 * Assembles `<li>` element for favourite list and appends it to list
 *
 * @param thread {Thread} Thread data from localStorage
 */
function assembleFavouriteRow(thread) {
    let row = $("<li></li>");
    row.attr("id", thread.thread + thread.boardAlias);
    row.addClass("d-flex border-bottom my-1 favourite");

    let deleteBtn = $("<button></button>");
    deleteBtn.addClass("favouriteDelete mr-1");
    deleteBtn.bind("click", () => {
        addFavourite(thread.boardAlias, thread.thread);
    });

    let updateBtn = $('<button></button>');
    updateBtn.addClass("favouriteUpdate mr-3 updateBtn");
    updateBtn.bind("click", async () => {
        let rotateInterval = updateBtnRotation(updateBtn[0]);
        //update thread data
        // thread = await thread.updateThreadData(thread.boardAlias, thread.thread);
        thread.unseenPosts = await thread.getUnseenPostsCount();
        //update counter
        let counter = $("#"+thread.thread + thread.boardAlias).find("span");
        if(thread.unseenPosts > 0){
            counter.text(thread.unseenPosts);
        }
        clearInterval(rotateInterval);
        updateBtn.css("transform", "rotate(0deg)");
    });

    let counter = $('<span></span>');
    if (thread.unseenPosts != 0) {
        counter.text(thread.unseenPosts);
    }
    counter.css("width", "auto");

    let theme = $("<span></span>");
    theme.text(thread.theme);

    let threadLink = $("<a></a>");
    threadLink.addClass("ml-2");
    threadLink.attr("href", location.origin+`/${thread.boardAlias}/res/${thread.thread}#${thread.lastSeenPost}`);
    threadLink.text(`/${thread.boardAlias}/${thread.thread} - ${theme.text()}`);

    row.append(deleteBtn, updateBtn, counter, threadLink);

    $("#favourites ul").append(row);
}

/**
 * Creates element with attachment
 *
 * @param json {JSON} Json-response with attachments
 * @param container {jQuery} Parent container for attachment
 * @return {HTMLVideoElement|HTMLImageElement}
 */
function createAttachment(json, container) {
    let attachment;
    if (json.attachment.mimeType.includes("video")) {
        attachment = $("<video></video>");
        attachment.attr("loop", true);
        attachment.attr("autoplay", true);
        attachment.attr("controls", true);
    } else {
        attachment = $("<img>");
    }
    attachment.attr("id", "attachment" + json.post_id + json.attachment_id + "Header");
    attachment.appendTo(container);
    attachment.css({
        "height": "98%",
        "width": "auto",
        "background-color": "grey",
        "padding": "10px 10px 0",
        "border-radius": "5px 5px 0 0"
    });
    attachment.attr("src", "data:" + json.attachment.mimeType + ";base64," + json.attachment.base64src);

    return attachment;
}

/**
 * Creates close btn for container
 *
 * @param container {jQuery}
 * @return {HTMLElement} <small>
 */
function createCloseBtn(container) {
    let closeBtn = $("<small></small>");
    closeBtn.text("Click to hide");
    closeBtn.appendTo(container);
    closeBtn.addClass("mx-auto");
    closeBtn.css({
        "cursor": "pointer",
        "height": "19px",
        "width": "100%",
        "background-color": "grey",
        "text-align": "center",
        "border-radius": "0 0 5px 5px",
    });

    closeBtn.click(() => {
        container.remove();
        $("document").off("scroll", "document", preventDefault);
    });

    return closeBtn;
}

/**
 * Creates container for attachment
 *
 * @param json {JSON} ajax-response
 * @return {jQuery} <div>
 */
function createAttachmentContainer(json) {
    const attachmentContainer = $("<div></div>");
    attachmentContainer.attr('id', "attachment" + json.post_id + json.attachment_id);
    attachmentContainer.css({
        "position": "fixed",
        "top":"50%",
        "left":"50%",
        "transform": "translate(-50%, -50%)",
        "z-index": "10",
    });
    //dimensions[width, height]
    const aspectRatio = json.attachment.dimensions[0] / json.attachment.dimensions[1];
    calculateAttachmentContainerSize(attachmentContainer, json.attachment.dimensions, aspectRatio);
    attachmentContainer.addClass("d-flex flex-column");

    attachmentContainer.bind({
        mousewheel: (event) => {
            preventDefault(event.originalEvent);
            resizeAttachment(event.originalEvent, aspectRatio, attachmentContainer);
        }
    });

    return attachmentContainer;
}

/**
 * Calculates and resizes container size to prevent screen overflowing
 *
 * @param {jQuery} attachmentContainer
 * @param {Array} dimensions Attachment dimensions
 * @param {Number} aspectRatio Attachment dimensions ratio
 */
function calculateAttachmentContainerSize(attachmentContainer, dimensions, aspectRatio) {
    let height;
    if (aspectRatio <= 1) { //height is bigger
        if (dimensions[1] >= window.innerHeight) {
            attachmentContainer.css("height", window.innerHeight + "px");
        } else {
            attachmentContainer.css("height", dimensions[1] + "px");
        }
        attachmentContainer.css("width", "auto");
    } else {
        if (dimensions[0] >= window.innerWidth) {
            attachmentContainer.css("width", window.innerWidth + "px");
            let windowHeightRatio = window.innerWidth / dimensions[0];
            height = windowHeightRatio * dimensions[1];
            if (height > window.innerHeight) { //if attachment still overflowing screen
                let newHeightRatio = window.innerHeight / height;
                height = newHeightRatio * height;
                attachmentContainer.css("width", window.innerWidth * newHeightRatio + "px");
            }
        } else {
            attachmentContainer.css("width", dimensions[0] + "px");
            height = dimensions[1];
        }
        attachmentContainer.css("height", height + "px");
    }
}