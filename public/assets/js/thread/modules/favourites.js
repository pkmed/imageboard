/** @var {Favourites} favourites */
let favourites;

/**
 * Initializes `favourites` in local storage, fills favourites list
 */
async function initFavourites() {
    let btn = $(".updateBtn[type=button]");
    let rotateInterval = updateBtnRotation(btn);
    if (!localStorage.getItem("favourites")) {
        favourites = new Favourites();
    } else {
        favourites = Object.assign(new Favourites(), JSON.parse(localStorage.getItem("favourites")));
        for (const board of Object.keys(favourites)) {
            for (let thread of Object.keys(favourites[board])) {
                //set Thread type to object
                favourites[board][thread] = Object.assign(new Thread(), favourites[board][thread]);
                assembleFavouriteRow(favourites[board][thread]);
                paintFavouriteBtn(thread, board);
            }
        }
        for (const board of Object.entries(favourites)) {
            for (let thread of Object.entries(board[1])) {
                let interval = updateBtnRotation($("#favouritesUl li#"+thread[0]+board[0]+" button.updateBtn")[0]);
                //update thread data with server ones
                //thread[1] = await thread[1].updateThreadData(board[0], parseInt(thread[0]));
                //update unseen posts count
                thread[1].unseenPosts = await thread[1].getUnseenPostsCount();
                clearInterval(interval);
                $("#favouritesUl li#"+thread[0]+board[0]+" button.updateBtn").css("transform", "rotate(0deg)");
            }
        }
        favourites.pushToStorage();
        refreshFavouritesList();
    }
    clearInterval(rotateInterval);
    btn.css("transform", "rotate(0deg)");
}

/**
 *
 * Adds or removes thread to favourite list
 *
 * @param alias {string} Board alias
 * @param opPostId {number} Id of op-post
 *
 * @see Favourites.addFavourite
 * @see Favourites.removeFavourite
 */
async function addFavourite(alias, opPostId) {
    if (favourites[alias]) {
        let same = favourites.getFavourite(alias, opPostId);
        if (!Boolean(same)) {//if that thread isn't exist in favourites
            //add
            await favourites.addFavourite(alias, opPostId);
            assembleFavouriteRow(favourites.getFavourite(alias, opPostId));
        } else {
            //remove
            favourites.removeFavourite(alias, opPostId);
            document.getElementById(`${opPostId}${alias}`).remove();
        }
    } else {
        //add
        await favourites.addFavourite(alias, opPostId);
        assembleFavouriteRow(favourites.getFavourite(alias, opPostId));
    }
    paintFavouriteBtn(opPostId, alias);
    favourites.pushToStorage();
}

/**
 * Handler for favourites list update button
 *
 * @param btn {Element} List update button
 * @return {Promise<void>}
 */
async function updateFavouritesBtnHandler(btn) {
    let rotateInterval = updateBtnRotation(btn);
    await favourites.updateFavourites();
    // refreshFavouritesList();
    clearInterval(rotateInterval);
    btn.style.transform = "rotate(0deg)";
}

/**
 * Updates every row in favourites list
 */
function refreshFavouritesList() {
    document.querySelector("#favourites ul").innerHTML = '';
    for (const board of Object.keys(favourites)) {
        for (let thread of Object.keys(favourites[board])) {
            //paintFavouriteBtn(parseInt(thread), board);
            assembleFavouriteRow(favourites[board][thread]);
        }
    }
}

/**
 * Updates last seen post on reaching bottom of thread
 */
function updateLastSeen() {
    //get last seen post
    let currentBoard = location.pathname.split('/')[1];
    let currentThread = location.pathname.split('/')[3];
    let lastPost = document.querySelector('#postList').lastElementChild.id;
    //get thread from favourites
    let favouriteThread = favourites.getFavourite(currentBoard, parseInt(currentThread));
    //compare last seen with last post
    if (parseInt(lastPost) !== favouriteThread.lastSeenPost) {
        favouriteThread.lastSeenPost = parseInt(lastPost);
        favouriteThread.lastPost = parseInt(lastPost);
        favouriteThread.unseenPosts = 0;
        refreshFavouritesList();
        favourites.pushToStorage();
    }
}

/**
 * Switches color of 'favourite' btn
 *
 * @param thread {number} Id of op-post
 * @param alias {string} Board alias
 */
function paintFavouriteBtn(thread, alias) {
    if (location.pathname.includes(alias)) {
        let opPost = document.getElementById(thread);
        if (opPost != null) {
            let btn = opPost.querySelector('.postAddFavourite');
            if (btn.style.backgroundColor != 'rgb(255, 191, 0)') {
                btn.style.backgroundColor = '#ffbf00';
            } else {
                btn.style.backgroundColor = '';
            }
        }
    }
}