/**
 * Contains
 *
 * @class Thread
 */
class Thread {
    constructor(boardAlias, lastPost, lastSeenPost, theme, thread, unseenPosts = 0) {
        this._boardAlias = boardAlias;
        this._lastPost = lastPost;
        this._lastSeenPost = lastSeenPost;
        this._theme = theme;
        this._thread = thread;
        this._unseenPosts = unseenPosts;
    }

    get boardAlias() {
        return this._boardAlias;
    }

    set boardAlias(boardAlias) {
        this._boardAlias = boardAlias;
    }

    get lastPost() {
        return this._lastPost;
    }

    set lastPost(lastPost) {
        this._lastPost = lastPost;
    }

    get lastSeenPost() {
        return this._lastSeenPost;
    }

    set lastSeenPost(lastSeenPost) {
        this._lastSeenPost = lastSeenPost;
    }

    get theme() {
        return this._theme;
    }

    set theme(theme) {
        this._theme = theme;
    }

    get thread() {
        return this._thread;
    }

    set thread(thread) {
        this._thread = thread;
    }

    get unseenPosts() {
        return this._unseenPosts;
    }

    set unseenPosts(unseenPosts) {
        this._unseenPosts = unseenPosts;
    }

    /**
     * Updates thread with server received data
     *
     * @param boardAlias {string} Board alias
     * @param threadId {number} Inboard id of OP-post of a thread
     * @return {Promise<Thread>} Thread instance with updated data
     */
    async updateThreadData(boardAlias, threadId) {
        let threadData = await executeFetch(loadOpPostUri(boardAlias, threadId));
        return Object.assign(this, threadData);
    }

    /**
     * Requests posts count from server, starting from lastSeenPost to lastPost
     *
     * @return {Promise<JSON|boolean|number>}
     * @see executeFetch
     */
    async getUnseenPostsCount() {
        return await executeFetch(getUnseenPostsUri(this.boardAlias, this.lastSeenPost));
    }
}
