/**
 * Contains logic for manipulating favourite threads in list
 *
 * @class Favourites
 */
class Favourites {
    constructor() {}

    /**
     * Adds Thread instance to favourites, access by this[boardAlias][threadId]
     *
     * @param boardAlias {string} Board alias
     * @param threadId {number} Inboard id of OP-post of a thread
     * @return {Promise<void>}
     */
    async addFavourite(boardAlias, threadId) {
        const threadData = await executeFetch(loadOpPostUri(boardAlias, threadId));
        threadData.lastSeenPost = threadData.lastPost;
        let favouriteThread = new Thread(
            threadData.board,
            threadData.lastPost,
            threadData.lastSeenPost,
            threadData.theme,
            threadData.thread
        );
        if (this[boardAlias]) {
            Object.assign(this[boardAlias], {[threadId]: favouriteThread});
        } else {
            this[boardAlias] = {[threadId]: favouriteThread};
        }
    }

    /**
     * Removes thread from favourites list
     *
     * @param boardAlias {string} Board alias
     * @param threadId {number} Inboard id of OP-post of a thread
     */
    removeFavourite(boardAlias, threadId) {
        delete this[boardAlias][threadId];
    }

    /**
     * Returns needed thread, if exists one
     *
     * @param boardAlias {string} Board alias
     * @param thread {number} Inboard id of OP-post of a thread
     * @return {null|Thread} Thread instance
     */
    getFavourite(boardAlias, thread) {
        if (this[boardAlias] && this[boardAlias][thread]) {
            return this[boardAlias][thread];
        } else {
            return null;
        }
    }

    /**
     * Pushes class' json into local storage
     */
    pushToStorage() {
        localStorage.setItem("favourites", JSON.stringify(this));
    }

    /**
     * Queries threads data from server
     *
     * @return {Promise<void>}
     */
    async updateFavourites(){
        for (const board of Object.keys(this)) {
            for (let thread in this[board]) if (this[board].hasOwnProperty(thread)) {
                // this[board][thread] = await this[board][thread].updateThreadData(board, thread);
                this[board][thread].unseenPosts = await this[board][thread].getUnseenPostsCount();
            }
        }
        this.pushToStorage();
    }
}
