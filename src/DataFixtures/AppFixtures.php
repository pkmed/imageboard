<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\Thread;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private const POSTS = [
        [
            'sender' => 'Anonymous',
            'text' => 'corrosive tainted by my sin',
            'theme' => 'testTheme',
        ],
        [
            'sender' => 'Anonymous',
            'text' => 'i`m spillin` blood and i can`t',
        ],
        [
            'sender' => 'Anonymous',
            'text' => 'hardly contain it',
        ],
        [
            'sender' => 'Anonymous',
            'text' => 'corrosive hallow in your hands',
        ],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $posts = $this->loadPosts($manager);
        $this->loadThreads($manager, $posts[0], $posts);

        $manager->flush();
    }

    private function loadPosts(ObjectManager $manager)
    {
        $posts = [];
        foreach (self::POSTS as $postData) {
            $post = new Post();
            $post->setSender($postData['sender']);
            $post->setTime(date("y/m/d D H:i:s"));
            $post->setText($postData['text']);
            $post->setTheme($postData['theme'] ?? null);
            $posts[] = $post;
            $manager->persist($post);
        }

        return $posts;
    }

    private function loadThreads(ObjectManager $manager, Post $op, array $posts)
    {
        $thread = new Thread();
        $thread->setOpPost($op);
        $op->setIsOp(true);
        $manager->persist($thread);
        /* @var Post $post */
        foreach ($posts as $post) {
            $post->setThread($thread);
        }
    }
}
