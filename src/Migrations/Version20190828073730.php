<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190828073730 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE board (alias VARCHAR(4) NOT NULL, name VARCHAR(32) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(alias)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE thread ADD board_alias VARCHAR(4) DEFAULT NULL, DROP board');
        $this->addSql('ALTER TABLE thread ADD CONSTRAINT FK_31204C8343D18CBF FOREIGN KEY (board_alias) REFERENCES board (alias) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_31204C8343D18CBF ON thread (board_alias)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C8343D18CBF');
        $this->addSql('DROP TABLE board');
        $this->addSql('DROP INDEX IDX_31204C8343D18CBF ON thread');
        $this->addSql('ALTER TABLE thread ADD board INT DEFAULT NULL, DROP board_alias');
    }
}
