<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190822053012 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE thread (id INT AUTO_INCREMENT NOT NULL, op_post_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_31204C836926C966 (op_post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, thread_id INT DEFAULT NULL, sender VARCHAR(255) DEFAULT NULL, time VARCHAR(255) DEFAULT NULL, text VARCHAR(15000) DEFAULT NULL, images JSON DEFAULT NULL, is_op TINYINT(1) NOT NULL, INDEX IDX_5A8A6C8DE2904019 (thread_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_answers (post_id INT NOT NULL, answer_id INT NOT NULL, INDEX IDX_6D3871354B89032C (post_id), INDEX IDX_6D387135AA334807 (answer_id), PRIMARY KEY(post_id, answer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE thread ADD CONSTRAINT FK_31204C836926C966 FOREIGN KEY (op_post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE post_answers ADD CONSTRAINT FK_6D3871354B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_answers ADD CONSTRAINT FK_6D387135AA334807 FOREIGN KEY (answer_id) REFERENCES post (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8DE2904019');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C836926C966');
        $this->addSql('ALTER TABLE post_answers DROP FOREIGN KEY FK_6D3871354B89032C');
        $this->addSql('ALTER TABLE post_answers DROP FOREIGN KEY FK_6D387135AA334807');
        $this->addSql('DROP TABLE thread');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE post_answers');
    }
}
