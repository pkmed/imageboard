<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Board", inversedBy="posts")
     * @ORM\JoinColumn(referencedColumnName="alias", nullable=true)
     */
    private $board;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $theme;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var ?string
     */
    private $sender;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $time;
    /**
     * @ORM\Column(type="string", length=15000, nullable=true)
     * @var ?string
     */
    private $text;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Post", mappedBy="answers")
     */
    private $references;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Post", inversedBy="references", cascade={"persist"})
     * @ORM\JoinTable(name="post_answers",
     *     joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="answer_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $answers;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $thumbnails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Attachment", mappedBy="post", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="post", referencedColumnName="id", onDelete="CASCADE")
     */
    private $attachments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Thread", inversedBy="posts", cascade={"persist"})
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $thread;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isOp = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostReport", mappedBy="post")
     */
    private $reports;

    /**
     * @ORM\Column(nullable=true, type="integer")
     */
    private $inboardId;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->references = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     */
    public function setSender(string $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return string|null
     */
    public function getTime(): ?string
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return ArrayCollection
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * @param Post $reference
     */
    public function setReference(Post $reference): void
    {
        $this->references->add($reference);
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param Post $answer
     */
    public function setAnswer(Post $answer): void
    {
        $this->answers->add($answer);
    }

    /**
     * @return string Json-array
     */
    public function getThumbnails()
    {
        return $this->thumbnails;
    }

    /**
     * @param string $thumbnails Json-array
     */
    public function setThumbnails($thumbnails): void
    {
        $this->thumbnails = $thumbnails;
    }

    /**
     * @return Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param Thread $thread
     */
    public function setThread($thread): void
    {
        $this->thread = $thread;
    }

    /**
     * @return bool
     */
    public function isOp()
    {
        return $this->isOp;
    }

    /**
     * @param bool $isOp
     */
    public function setIsOp($isOp): void
    {
        $this->isOp = $isOp;
    }

    /**
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param string $theme
     */
    public function setTheme($theme): void
    {
        $this->theme = $theme;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param ArrayCollection $attachments
     */
    public function setAttachments($attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * @param mixed $reports
     */
    public function setReports($reports): void
    {
        $this->reports = $reports;
    }

    /**
     * @return Board
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * @param Board $board
     */
    public function setBoard($board): void
    {
        $this->board = $board;
    }

    /**
     * @return mixed
     */
    public function getInboardId()
    {
        return $this->inboardId;
    }

    /**
     * @param mixed $inboardId
     */
    public function setInboardId($inboardId): void
    {
        $this->inboardId = $inboardId;
    }
}
