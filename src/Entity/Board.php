<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoardRepository")
 */
class Board
{
    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $name;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Thread", mappedBy="board", cascade={"remove"})
     */
    private $threads;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BoardTheme", inversedBy="boards")
     * @ORM\JoinColumn(name="general_theme", referencedColumnName="id", onDelete="SET NULL")
     */
    private $boardTheme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postsTotal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="board", cascade={"remove"})
     */
    private $posts;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * @param ArrayCollection $threads
     */
    public function setThreads($threads): void
    {
        $this->threads = $threads;
    }

    /**
     * @return BoardTheme
     */
    public function getBoardTheme()
    {
        return $this->boardTheme;
    }

    /**
     * @param BoardTheme $boardTheme
     */
    public function setBoardTheme($boardTheme): void
    {
        $this->boardTheme = $boardTheme;
    }

    /**
     * @return mixed
     */
    public function getPostsTotal()
    {
        return $this->postsTotal;
    }

    /**
     * @return void
     */
    public function appendPost(): void
    {
        $this->postsTotal = ++$this->postsTotal;
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post[]|ArrayCollection $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

}
