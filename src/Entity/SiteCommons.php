<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteCommonsRepository")
 */
class SiteCommons
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(
     *     mimeTypes={"image/gif", "image/jpeg", "image/svg+xml", "image/svg", "image/png", "image/tiff"},
     *     mimeTypesMessage="Image must be of gitf jpeg, svg, png or tiff extension!"
     * )
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siteName;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPosts;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalThreads;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $copyright;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disclaimer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }

    public function getTotalPosts(): ?int
    {
        return $this->totalPosts;
    }

    public function setTotalPosts(int $totalPosts): self
    {
        $this->totalPosts = $totalPosts;

        return $this;
    }

    public function getTotalThreads(): ?int
    {
        return $this->totalThreads;
    }

    public function setTotalThreads(int $totalThreads): self
    {
        $this->totalThreads = $totalThreads;

        return $this;
    }

    public function getCopyright(): ?string
    {
        return $this->copyright;
    }

    public function setCopyright(string $copyright): self
    {
        $this->copyright = $copyright;

        return $this;
    }

    public function getDisclaimer(): ?string
    {
        return $this->disclaimer;
    }

    public function setDisclaimer(string $disclaimer): self
    {
        $this->disclaimer = $disclaimer;

        return $this;
    }
}
