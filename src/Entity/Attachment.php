<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttachmentRepository")
 */
class Attachment
{
    const LOAD = 'attach.load';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="attachments", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $post;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({Attachment::LOAD})
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({Attachment::LOAD})
     */
    private $fileSize;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({Attachment::LOAD})
     */
    private $mimeType;

    /**
     * @ORM\Column(type="blob", nullable=false)
     * @Groups({Attachment::LOAD})
     */
    private $base64src;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({Attachment::LOAD})
     */
    private $dimensions;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param int $fileSize
     */
    public function setFileSize($fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType($mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return string Base64 string
     */
    public function getBase64src()
    {
        return stream_get_contents($this->base64src);
    }

    /**
     * @param string $base64src Base64 string
     */
    public function setBase64src($base64src): void
    {
        $this->base64src = $base64src;
    }

    /**
     * @return array [width, height]
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param mixed $dimensions [width, height]
     */
    public function setDimensions($dimensions): void
    {
        $this->dimensions = $dimensions;
    }

}
