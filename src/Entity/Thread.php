<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThreadRepository")
 */
class Thread
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="thread")
     */
    private $posts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Post", cascade={"remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="op_post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $opPost;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $deletionMark = false;

    /**
     * @ORM\Column(type="smallint", length=1000, nullable=false)
     */
    private $postNumber;

    /**
     * @ORM\Column(type="smallint", length=1000, nullable=false)
     */
    private $bumpLimit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastUpdate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Board", inversedBy="threads")
     * @ORM\JoinColumn(name="board_alias", referencedColumnName="alias", onDelete="CASCADE")
     */
    private $board;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $pin = false;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int  $offset From where post should gotten
     * @param null $length Posts number
     * @return ArrayCollection|array
     */
    public function getPosts($offset = 0, $length = null)
    {
        return $length != null ? $this->posts->slice($offset, $length) : $this->posts;
    }

    /**
     * @return Post
     */
    public function getOpPost()
    {
        return $this->opPost;
    }

    /**
     * @param Post $opPost
     */
    public function setOpPost(Post $opPost): void
    {
        $this->opPost = $opPost;
    }

    /**
     * @return bool
     */
    public function isMarkedToDelete()
    {
        return $this->deletionMark;
    }

    /**
     * @param bool $deletionMark
     */
    public function markToDelete($deletionMark): void
    {
        $this->deletionMark = $deletionMark;
    }

    /**
     * @return int
     */
    public function getPostNumber()
    {
        return $this->postNumber;
    }

    /**
     * @param int $postNumber
     */
    public function setPostNumber($postNumber): void
    {
        $this->postNumber = $postNumber;
    }

    /**
     * @return int
     */
    public function getBumpLimit()
    {
        return $this->bumpLimit;
    }

    /**
     * @param int $bumpLimit
     */
    public function setBumpLimit($bumpLimit): void
    {
        $this->bumpLimit = $bumpLimit;
    }

    /**
     * @return string 'y/m/d D H:i:s'
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @param string $lastUpdate
     */
    public function setLastUpdate($lastUpdate): void
    {
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return Board
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * @param Board $board
     */
    public function setBoard($board): void
    {
        $this->board = $board;
    }

    /**
     * @return bool
     */
    public function isPin()
    {
        return $this->pin;
    }

    /**
     * @param bool $pin
     */
    public function pin($pin): void
    {
        $this->pin = $pin;
    }
}
