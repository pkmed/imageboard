<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AppExtensions
 *
 * @package App\Twig
 */
class AppExtensions extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('pull', [$this, 'pull']),
            new TwigFilter('contain', [$this, 'contain']),
            new TwigFilter('ucfirst', [$this, 'ucfirst']),
        ];
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('attachReferences', [$this, 'attachReferences']),
        ];
    }

    /**
     * Attaching references of certain posts marks in text
     *
     * @param string|null $text Post text
     * @param string      $ajax Link for ajax-query
     * @param string      $id   Post id
     * @return mixed|string|null
     */
    public function attachReferences(?string $text, string $ajax, string $id)
    {
        //extract all numbers of referenced posts
        $test = explode(" ", str_replace(PHP_EOL, ' ', $text));
        $referencesNumbers = array_filter(
            $test,
            function ($entry) {
                return mb_strcut($entry, 0, 2) == '>>';
            }
        );
        foreach ($referencesNumbers as $refNumber) {
            //$number - id of referenced post
            $number = trim(mb_strcut($refNumber, 2));
            $i = 0;
            while ($i < strlen($number) && is_numeric($number[$i])) {
                $i++; //counter for cases ">>19483249fakjdsfij"
            }
            $number = mb_strcut($number, 0, $i);
            $refNumber = mb_strcut($refNumber, 0, $i + 2);
            $path = mb_strcut($ajax, 0, -1).$number; // cuts mock post id from url for ajax
            $template = "<a id=\"{$number}_{$id}\" href='#{$number}' onmouseover=\"showPreview('{$number}_{$id}', '{$path}')\" onclick=\"highlightReferenced('{$number}')\">"
                .trim($refNumber)."</a>";
            $text = str_replace($refNumber, $template, $text);
        }

        return $text;
    }

    /**
     * Pull entity by $id from $haystack
     *
     * @param array $haystack Array where to search
     * @param int   $id       Id of entity which needed
     * @return object|false Found entity or false
     */
    public function pull(array $haystack, int $id)
    {
        foreach ($haystack as $entity) {
            if ($entity->getId() == $id) {
                return $entity;
            }
        }

        return false;
    }

    /**
     * Check if array contains some value
     *
     * @param array $haystack Array where to search
     * @param mixed $needle   Value
     * @return bool
     */
    public function contain(array $haystack, $needle)
    {
        if (array_search($needle, $haystack)) {
            return true;
        }

        return false;
    }

    /**
     * Function-wrapper for ucfirst()
     *
     * @param string $string
     * @return string
     */
    public function ucfirst(string $string)
    {
        return ucfirst($string);
    }

}