<?php

namespace App\Form;

use App\Controller\BoardController;
use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PostType
 *
 * @package App\Form
 */
class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['attr']['mode'] == BoardController::FORM_MODE_START_THREAD) {
            $builder->add('theme', TextType::class, ['required' => false]);
        } else {
            $builder->add('isOp', CheckboxType::class, ['required' => false])
                ->add('sage', CheckboxType::class, ['required' => false, 'mapped' => false]);
        }
        $builder
            ->add('sender', TextType::class, ['required' => false, 'empty_data' => 'Anonymous'])
            ->add(
                'text',
                TextareaType::class
            )
            ->add(
                'attachments',
                FileType::class,
                [
                    'multiple' => true,
                    'allow_extra_fields' => true,
                    'mapped' => true,
                    'required' => false,
                    'attr' => [
                        'accept' => 'image/*, video/webm, video/mpeg, video/mp4',
                    ],
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Post::class,
                'csrf_protection' => false,
            ]
        );
    }
}
