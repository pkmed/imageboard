<?php

namespace App\Form;

use App\Entity\Board;
use App\Entity\BoardTheme;
use App\Repository\BoardRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BoardThemeType
 *
 * @package App\Form
 */
class BoardThemeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $boardThemeId = $options['attr']['boardThemeId'] ?? -1;
        $builder->add('name', TextType::class, ['required' => true])
            ->add(
                'boards',
                EntityType::class,
                [
                    'query_builder' => function (BoardRepository $repo) use ($boardThemeId) {
                        return $repo->createQueryBuilder('b')
                            ->where('b.boardTheme=:themeId')
                            ->setParameter('themeId', $boardThemeId);
                    },
                    'class' => Board::class,
                    'choice_label' => 'name',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BoardTheme::class,
                'csrf_protection' => false,
            ]
        );
    }

}