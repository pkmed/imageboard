<?php

namespace App\Form;

use App\Entity\Board;
use App\Entity\BoardTheme;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BoardType
 *
 * @package App\Form
 */
class BoardType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['required' => true])
            ->add('alias', TextType::class, ['required' => true])
            ->add('description', TextType::class, ['required' => true])
            ->add(
                'boardTheme',
                EntityType::class,
                [
                    'class' => BoardTheme::class,
                    'choice_label' => 'name',
                    'required' => true,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Board::class,
                'csrf_protection' => false,
            ]
        );
    }

}