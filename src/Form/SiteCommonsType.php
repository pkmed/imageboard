<?php

namespace App\Form;

use App\Entity\SiteCommons;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SiteCommonsType
 *
 * @package App\Form
 */
class SiteCommonsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('siteName')
            ->add('logo', FileType::class, ['mapped' => false, 'required' => false])
            ->add('disclaimer', TextareaType::class)
            ->add('copyright');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SiteCommons::class,
                'csrf_protection' => false,
            ]
        );
    }

}