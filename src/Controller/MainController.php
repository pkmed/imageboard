<?php

namespace App\Controller;

use App\Repository\BoardThemeRepository;
use App\Repository\SiteCommonsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @var BoardThemeRepository
     */
    public $themeRepository;
    /**
     * @var SiteCommonsRepository
     */
    public $siteCommonsRepository;

    public function __construct(BoardThemeRepository $themeRepository, SiteCommonsRepository $siteCommonsRepository)
    {
        $this->themeRepository = $themeRepository;
        $this->siteCommonsRepository = $siteCommonsRepository;
    }

    /**
     * @Route("/", name="main_page")
     * @return Response
     */
    public function index()
    {
        $themes = $this->themeRepository->findAll();

        foreach ($themes as $theme) {
            if ($theme->getBoards()->count() > 0) {
                $themesActive[] = $theme;
            }
        }

        $commons = $this->siteCommonsRepository->findAll()[0];

        return $this->render(
            'main.html.twig',
            [
                'themes' => $themesActive ?? [],
                'siteCommons' => $commons,
            ]
        );
    }
}