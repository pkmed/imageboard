<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Board;
use App\Entity\Post;
use App\Entity\Thread;
use App\Form\EditOpFormType;
use App\Form\PostType;
use App\Repository\BoardRepository;
use App\Repository\BoardThemeRepository;
use App\Repository\PostReportRepository;
use App\Repository\PostRepository;
use App\Repository\SiteCommonsRepository;
use App\Repository\ThreadRepository;
use Doctrine\DBAL\DBALException;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Imagick;
use ImagickException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BoardController
 *
 * @Route("/{alias<[a-z]{1,4}>}")
 *
 * @package App\Controller
 */
class BoardController extends AbstractController
{
    /**
     * @var int $FORM_MODE_CREATE_POST Post form mode
     */
    public const FORM_MODE_CREATE_POST = 0;
    /**
     * @var int $FORM_MODE_START_THREAD Post form mode
     */
    public const FORM_MODE_START_THREAD = 1;
    /**
     * @var ThreadRepository
     */
    public $threadRepository;
    /**
     * @var FormFactoryInterface
     */
    public $formFactory;
    /**
     * @var PostRepository
     */
    public $postRepository;
    /**
     * @var BoardRepository
     */
    public $boardRepository;
    /**
     * @var BoardThemeRepository
     */
    public $themeRepository;
    /**
     * @var PostReportRepository
     */
    public $reportRepository;
    /**
     * @var SiteCommonsRepository
     */
    public $siteCommonsRepository;

    public function __construct(
        BoardThemeRepository $themeRepository,
        BoardRepository $boardRepository,
        ThreadRepository $threadRepository,
        PostRepository $postRepository,
        FormFactoryInterface $formFactory,
        PostReportRepository $reportRepository,
        SiteCommonsRepository $siteCommonsRepository
    ) {
        $this->threadRepository = $threadRepository;
        $this->formFactory = $formFactory;
        $this->postRepository = $postRepository;
        $this->boardRepository = $boardRepository;
        $this->themeRepository = $themeRepository;
        $this->reportRepository = $reportRepository;
        $this->siteCommonsRepository = $siteCommonsRepository;
    }

    /**
     * Threads list and processing thread-create form
     *
     * @Route("/", name="board_index")
     *
     * @param Board   $board   Board instance
     * @param Request $request Request instance
     *
     * @return bool|RedirectResponse|Response
     *
     * @throws DBALException
     * @throws ImagickException
     */
    public function board(Board $board, Request $request)
    {
        $vars = $this->plainVariables(self::FORM_MODE_START_THREAD);

        $vars['postForm']->handleRequest($request);

        if ($vars['postForm']->isSubmitted()) {
            $result = $this->processPost($vars['postForm'], $board);
            if ($result) {
                return new RedirectResponse(
                    $this->generateUrl('board_index', ['alias' => $board->getAlias()])
                );
            }
        }

        $threads = $this->threadRepository->find20RecentUpdated($board->getAlias());

        foreach ($threads as $thread) {
            /* @var Thread $thread */
            $posts[] = [
                'op' => $thread->getOpPost(),
                'last' => $thread->getPosts()->last(),
            ];
        }

        return $this->render(
            'thread/index.html.twig',
            [
                'themes' => $vars['themesActive'] ?? [],
                'commons' => $vars['siteCommons'],
                'board' => $board,
                'posts' => $posts ?? [],
                'reports' => $vars['reports'],
                'postForm' => $vars['postForm']->createView(),
                'editForm' => $this->isGranted('ROLE_MODER') ? $vars['editForm']->createView() : null,
                'mode' => self::FORM_MODE_START_THREAD,
            ]
        );
    }

    /**
     * Show thread page and process post-create form
     *
     * @Route("/res/{op_post_id<\d+>}", name="thread_index")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id": "inboardId"}})
     *
     * @param Board   $board   Board instance
     * @param Post    $opPost  Post instance
     * @param Request $request Request instance
     *
     * @return Response
     *
     * @throws DBALException
     * @throws ImagickException
     */
    public function thread(Board $board, Post $opPost, Request $request)
    {
        $vars = $this->plainVariables(self::FORM_MODE_CREATE_POST);
        if (!$opPost->isOp()) {
            throw new NotFoundHttpException('Page not found', null, Response::HTTP_NOT_FOUND);
        }

        $vars['postForm']->handleRequest($request);

        $postsNumber = $opPost->getThread()->getPosts()->count();

        if ($vars['postForm']->isSubmitted()) {
            $result = $this->processPost($vars['postForm'], $board, $opPost->getThread());
            if ($result) {
                $postModel = $vars['postForm']->getData();
                $post = $this->renderView(
                    'thread/post.html.twig',
                    [
                        'post' => $postModel,
                        'postLoop' => ['index' => count($opPost->getThread()->getPosts()) + 1],
                        'isPreview' => false,
                    ]
                );

                return new JsonResponse(['post' => $post]);
            } else {
                $errors = [];
                /** @var FormError $error */
                foreach ($vars['postForm']->getErrors() as $error) {
                    $errors[] = $error->getMessage();
                }

                return new JsonResponse(['formErrors' => $errors], Response::HTTP_OK);
            }
        }

        $posts = $opPost->getThread()->getPosts(0, 15);

        return $this->render(
            'thread/index.html.twig',
            [
                'themes' => $vars['themesActive'] ?? [],
                'commons' => $vars['siteCommons'],
                'board' => $board,
                'posts' => $posts,
                'reports' => $vars['reports'],
                'postForm' => $vars['postForm']->createView(),
                'editForm' => $this->isGranted('ROLE_MODER') ? $vars['editForm']->createView() : null,
                'mode' => self::FORM_MODE_CREATE_POST,
                'postsNumber' => $postsNumber,
            ]
        );
    }

    /**
     * Editing op post's sender, theme and text
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/res/{op_post_id<\d+>}/edit", name="thread_edit")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id": "inboardId"}})
     *
     * @param Post    $opPost  Post instance
     * @param Request $request Request instance
     *
     * @return RedirectResponse
     */
    public function editThreadHeader(Post $opPost, Request $request)
    {
        $vars = $this->plainVariables();
        $vars['editForm']->handleRequest($request);

        /** @var Post $fields */
        $fields = $vars['editForm']->getData();
        $bumpLimit = $vars['editForm']->getExtraData()['bumpLimit'];
        $opPost->setSender($fields->getSender());
        $opPost->setTheme($fields->getTheme());
        $opPost->setText($fields->getText());
        $opPost->getThread()->setBumpLimit($bumpLimit);

        $this->getDoctrine()->getManager()->persist($opPost);
        $this->getDoctrine()->getManager()->persist($opPost->getThread());
        $this->getDoctrine()->getManager()->flush();

        $prev = $request->headers->get('referer');
        if (!preg_match("~^(https|http)://[\w.:]+/[a-zA-Z]{1,4}/$~", $prev)) {
            return new RedirectResponse(
                $this->generateUrl(
                    'thread_index',
                    [
                        'alias' => $request->attributes->get('alias'),
                        'op_post_id' => $request->attributes->get('op_post_id') ?? null,
                    ]
                )
            );
        } else {
            return new RedirectResponse(
                $this->generateUrl(
                    'board_index',
                    [
                        'alias' => $request->attributes->get('alias'),
                    ]
                )
            );
        }
    }

    /**
     * Fasten thread at the top of list
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/pin/{op_post_id<\d+>}/", name="thread_pin")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id": "inboardId"}})
     *
     * @param Post    $opPost  Post instance
     * @param Request $request Request instance
     *
     * @return RedirectResponse
     */
    public function pinThread(Post $opPost, Request $request)
    {
        $thread = $opPost->getThread();
        $thread->pin(true);
        $this->getDoctrine()->getManager()->persist($thread);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse(
            $this->generateUrl('board_index', ['alias' => $request->get('alias')])
        );

    }

    /**
     * Unfasten thread from the top of list
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/unpin/{op_post_id<\d+>}/", name="thread_unpin")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id": "inboardId"}})
     *
     * @param Post    $opPost  Post instance
     * @param Request $request Request instance
     *
     * @return RedirectResponse
     */
    public function unpinThread(Post $opPost, Request $request)
    {
        $thread = $opPost->getThread();
        $thread->pin(false);
        $this->getDoctrine()->getManager()->persist($thread);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->generateUrl('board_index', ['alias' => $request->get('alias')]));

    }

    /**
     * Thread deletion
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/delete/thread/{op_post_id<\d+>}", name="thread_delete")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id": "inboardId"}})
     *
     * @param Post    $opPost  Post instance
     * @param Request $request Request instance
     *
     * @return RedirectResponse
     */
    public function deleteThread(Post $opPost, Request $request)
    {
        $thread = $opPost->getThread();
        $this->getDoctrine()->getManager()->remove($thread);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('notification', 'Thread was successfully deleted!');

        return new RedirectResponse($this->generateUrl('board_index', ['alias' => $request->get('alias')]));
    }

    /**
     * Post deletion
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/res/{op_post_id<\d+>}/delete/post/{id<\d+>}", name="post_delete")
     * @ParamConverter("post", options={"mapping": {"alias" : "alias", "id": "inboardId"}})
     *
     * @param Post    $post    Post instance
     * @param Request $request Request instance
     *
     * @return RedirectResponse
     */
    public function deletePost(Post $post, Request $request)
    {
        $this->getDoctrine()->getManager()->remove($post);
        $this->getDoctrine()->getManager()->persist($post->getThread());
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('notification', 'Post was successfully deleted!');

        return new RedirectResponse(
            $this->generateUrl(
                'thread_index',
                ['alias' => $request->get('alias'), 'op_post_id' => $request->get('op_post_id') ?? 0]
            )
        );
    }

    /**
     * Loads active themes and form depends on $formMode
     *
     * @param int $formMode Form mode from constants
     *
     * @return array[themesActive, postForm, editForm, reports]
     */
    private function plainVariables(int $formMode = null)
    {
        $vars['themesActive'] = $this->themeRepository->getNotEmptyThemes();

        $vars['postForm'] = $this->formFactory->create(
            PostType::class,
            new Post(),
            ['attr' => ['mode' => $formMode]]
        );

        $vars['editForm'] = $this->formFactory->create(
            EditOpFormType::class,
            new Post()
        );

        $vars['reports'] = $this->reportRepository->findAll();

        $vars['siteCommons'] = $this->siteCommonsRepository->findAll()[0];

        return $vars;
    }

    /**
     * Processing post depending on its type
     *
     * @param FormInterface $form   Processable form, either post-form or thread-form
     * @param Board         $board  Instance of board
     * @param Thread|null   $thread If null - then post will start a new thread
     *
     * @return bool Returns true on success
     *
     * @throws DBALException
     * @throws ImagickException
     */
    private function processPost(FormInterface $form, Board $board, Thread $thread = null)
    {
        if ($form->isValid()) {
            /* @var Post $post */
            $post = $form->getData();
            $commons = $this->siteCommonsRepository->findAll()[0];

            //attachments processing
            if ($post->getAttachments() != null) {
                if (!$this->validateAttachments($post, $form)) {
                    return false;
                }
                $attachments = $this->processAttachments($form->get('attachments')->getData(), $post);
                $post->setThumbnails($attachments['thumb']);
                $post->setAttachments($attachments['full']);
            }

            $post->setTime(date("y/m/d D H:i:s"));

            $post->setBoard($board);
            $board->appendPost();
            $post->setInboardId($board->getPostsTotal());

            if (is_null($thread)) {
                //thread creation
                $thread = $this->createThread($board, $post);
                $post->setIsOp(true);
                $post->setTheme($post->getTheme() ?? mb_strcut($post->getText(), 0, 25).'...');
                $commons->setTotalThreads($commons->getTotalThreads() + 1);
                $commons->setTotalPosts($commons->getTotalPosts() + 1);
            } else {
                $post->setTheme(null);
                if (!$form->get('sage')->getNormData()) {
                    $thread->setLastUpdate($post->getTime());
                }
                $commons->setTotalPosts($commons->getTotalPosts() + 1);
            }
            $this->processPostText($post);
            $post->setThread($thread);

            $this->getDoctrine()->getManager()->persist($post);
            if (isset($attachments)) {
                foreach ($attachments['full'] as $full) {
                    $this->getDoctrine()->getManager()->persist($full);
                }
            }
            $this->getDoctrine()->getManager()->persist($thread);
            $this->getDoctrine()->getManager()->flush();
            $thread->setPostNumber($thread->getPosts()->count());
            $this->getDoctrine()->getManager()->flush();

            $this->threadRepository->updateMarksToDeletion($thread->getBoard()->getAlias());

            return true;
        } else {
            return false;
        }
    }

    /**
     * Generates thumbnails for attachments, encodes both in base64, forms array for images
     *
     * @param array $attachments Array of images received from form
     * @param Post  $post        Post instance for attaching
     *
     * @return array ['thumb'=>[0=>[filename, size(KB), mime, base64]],
     *                           'full'=>[0=> Attachment entity]
     *                           Array of thumbnails and full images
     *
     * @throws ImagickException
     */
    private function processAttachments(array $attachments, Post $post)
    {
        /* @var UploadedFile $attachment */
        foreach ($attachments as $attachment) {
            $mime = $attachment->getClientMimeType();
            //thumbnail creating
            if (strstr($mime, 'image')) {
                $thumb = new Imagick($attachment->getRealPath());
                $thumb->thumbnailImage(240, 240, true);
                $dimensions[0] = getimagesize($attachment->getRealPath())[0];
                $dimensions[1] = getimagesize($attachment->getRealPath())[1];
            } elseif (strstr($mime, 'video')) {
                $video = FFMpeg::create()->open($attachment->getRealPath());
                $frame = $video->frame(TimeCode::fromSeconds(1));
                $tmp = tmpfile();
                $metas = stream_get_meta_data($tmp);
                $frame->save($metas['uri']);
                $thumb = new Imagick($metas['uri']);
                $thumb->thumbnailImage(180, 180, true);
                $dimensions[0] = $video->getFFProbe()->streams($attachment->getRealPath())->videos()->first(
                )->getDimensions()->getWidth();
                $dimensions[1] = $video->getFFProbe()->streams($attachment->getRealPath())->videos()->first(
                )->getDimensions()->getHeight();
            }

            $thumbs[] = [
                $attachment->getClientOriginalName(),
                round($attachment->getSize() / 1024, 0),
                'image/*',
                base64_encode($thumb->getImageBlob()),
                [$thumb->getImageWidth(), $thumb->getImageHeight()],
            ];

            $fulls[] = $this->createAttachment($attachment, $post, $dimensions ?? [0, 0]);
        }

        return ['thumb' => $thumbs ?? [], 'full' => $fulls ?? []];
    }

    /**
     * Creates Thread instance
     *
     * @param Board $board Board instance
     * @param Post  $post  Op post for this thread
     *
     * @return Thread
     */
    private function createThread(Board $board, Post $post)
    {
        $thread = new Thread();
        $thread->setBoard($board);
        $thread->setOpPost($post);
        $thread->setPostNumber(1);
        $thread->setBumpLimit(500);
        $thread->setLastUpdate($post->getTime());

        return $thread;
    }

    /**
     * Creates instance of Attachment
     *
     * @param UploadedFile $attachment Uploaded attachment
     * @param Post         $post       Post which attachment should be attached to
     * @param array        $dimensions Dimensions of attachment
     *
     * @return Attachment Attachment instance
     */
    private function createAttachment(UploadedFile $attachment, Post $post, array $dimensions)
    {
        $file = new Attachment();
        $file->setFilename($attachment->getClientOriginalName());
        $file->setFileSize(round($attachment->getSize() / 1024, 0));
        $file->setMimeType($attachment->getClientMimeType());
        $file->setBase64src(base64_encode(stream_get_contents(fopen($attachment->getRealPath(), 'r'))));
        $file->setPost($post);
        $file->setDimensions($dimensions);

        return $file;
    }

    /**
     * Attachments validation
     *
     * @param Post          $post Post instance
     * @param FormInterface $form FormInterface instance
     *
     * @return bool true on success
     */
    private function validateAttachments(Post $post, FormInterface $form)
    {
        $mimes = [
            'image/gif',
            'image/jpeg',
            'image/jpg',
            'image/png',
            'image/tiff',
            'video/webm',
            'video/mpeg',
            'video/mp4',
            'application/octet-stream',
        ];

        if (sizeof($post->getAttachments()) <= 4) {
            /* @var UploadedFile $attachment */
            foreach ($post->getAttachments() as $attachment) {
                if (is_bool(array_search($attachment->getClientMimeType(), $mimes))) {
                    $form->addError(new FormError('File must be an image or video(webm, mpeg, mp4)'));
                }
            }
        } else {
            $form->addError(new FormError('Only 4 files allowed'));
        }

        return $form->getErrors()->count() > 0 ? false : true;
    }

    /**
     * Processes post text
     *
     * @param Post $post Processable post
     */
    private function processPostText(Post $post)
    {
        $postText = explode(PHP_EOL, $post->getText());
        foreach ($postText as &$string) {
            $string = trim($string);
            $string = $this->highlightQuotes($string);
            $string = $this->wrapLinks($string);
        }
        $post->setText(implode(PHP_EOL, $postText));
        $this->setReferences($post);
        $this->processTags($post);

    }

    /**
     * Wraps all links in post text into 'a' tag
     *
     * @param string $string String from post text
     *
     * @return string
     */
    private function wrapLinks(string $string)
    {
        preg_match_all("~((https|http)://)?(w{0,3}\.?)(\w+)(\.\w+)(/[\w?!=&]*)*~", $string, $links);
        foreach ($links[0] as $link) {
            if (substr($link, 0, 4) != 'http') {
                $template = "<a href='http://{$link}'>{$link}</a>";
            } else {
                $template = "<a href='{$link}'>{$link}</a>";
            }
            $string = str_replace($link, $template, $string);
        }

        return $string;
    }

    /**
     * Finding quotes in text, wrapping it into 'span' tag
     *
     * @param string $string String from post text
     *
     * @return string String with wrapped quote
     */
    private function highlightQuotes(string $string)
    {
        if (preg_match("~^>[\d\w\s]+~", $string)) {
            $string = str_replace($string, " <span style='color: #2b5b00'>{$string}</span> ", $string);
        }

        return $string;
    }

    /**
     * Extracting post numbers from text, set references to mentioned posts
     *
     * @param Post $post Processing post
     */
    private function setReferences(Post $post)
    {
        //extract post number from $post text
        $referencesNumbers = array_filter(
            explode(" ", str_replace(PHP_EOL, ' ', $post->getText())),
            function ($entry) {
                return mb_strcut($entry, 0, 2) == '>>';
            }
        );
        foreach ($referencesNumbers as &$reference) {
            $reference = trim(mb_strcut($reference, 2));
            $i = 0;
            while ($i < strlen($reference) && is_numeric($reference[$i])) {
                $i++;
            }
            $reference = mb_strcut($reference, 0, $i);
        }
        //get post from db with this id
        $posts = $this->postRepository->findBy(
            ['inboardId' => $referencesNumbers, 'board' => $post->getBoard()->getAlias()]
        );
        foreach ($posts as $refPost) {
            //set received post as reference to current post
            $post->setReference($refPost);
            //set current post to received post as an answer
            $refPost->setAnswer($post);
        }
    }

    /**
     * Replaces inText tags([b],[i] e.t.c.) with html ones
     *
     * @param Post $post Processable post
     */
    private function processTags(Post $post)
    {
        $tags = [
            'bold' => [
                'inText' => ['[b]', '[/b]'],
                'replace' => ['<strong>', '</strong>'],
            ],
            'italic' => [
                'inText' => ['[i]', '[/i]'],
                'replace' => ['<i>', '</i>'],
            ],
            'underscore' => [
                'inText' => ['[u]', '[/u]'],
                'replace' => ['<u>', '</u>'],
            ],
            'overline' => [
                'inText' => ['[o]', '[/o]'],
                'replace' => ['<span class="overline">', '</span>'],
            ],
            'spoiler' => [
                'inText' => ['[spoiler]', '[/spoiler]'],
                'replace' => ['<span class="spoiler">', '</span>'],
            ],
            'strike' => [
                'inText' => ['[s]', '[/s]'],
                'replace' => ['<s>', '</s>'],
            ],
            'sup' => [
                'inText' => ['[sup]', '[/sup]'],
                'replace' => ['<sup>', '</sup>'],
            ],
            'sub' => [
                'inText' => ['[sub]', '[/sub]'],
                'replace' => ['<sub>', '</sub>'],
            ],
        ];

        $postText = $post->getText();

        foreach ($tags as $tag) {
            for ($i = 0; $i < 2; $i++) {
                $postText = str_replace($tag['inText'][$i], $tag['replace'][$i], $postText);
            }
        }

        $post->setText($postText);

    }
}