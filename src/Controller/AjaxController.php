<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Post;
use App\Entity\PostReport;
use App\Repository\PostReportRepository;
use App\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AjaxController
 *
 * @package App\Controller
 */
class AjaxController extends AbstractController
{
    /**
     * @var PostReportRepository
     */
    public $reportRepository;
    /**
     * @var PostRepository
     */
    public $postRepository;

    /**
     * AjaxController constructor.
     *
     * @param PostRepository       $postRepository
     * @param PostReportRepository $reportRepository
     */
    public function __construct(PostRepository $postRepository, PostReportRepository $reportRepository)
    {
        $this->reportRepository = $reportRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Report creating from UI
     *
     * @Route("/create/report/{alias<[a-z]{1,4}>}/{id<\d+>}", name="ajax_report")
     * @ParamConverter("post", options={"mapping": {"alias" : "board", "id": "inboardId"}})
     *
     * @param Post    $post
     * @param Request $request
     * @return JsonResponse
     */
    public function createReport(Post $post, Request $request)
    {
        $report = new PostReport();
        if ($post != null) {
            $report->setPost($post);
            $reason = $request->get('reason');
            $report->setReason($reason);
        } else {
            return new JsonResponse('Oops, there is some trouble!', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->getDoctrine()->getManager()->persist($report);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('Report was sent.');

    }

    /**
     * Report deleting from UI
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/delete/report/{id<\d+>}", name="ajax_delete_report")
     * @param PostReport $report
     * @return JsonResponse
     */
    public function deleteReport(PostReport $report)
    {
        $this->getDoctrine()->getManager()->remove($report);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("Report with reason:'{$report->getReason()}' was deleted!", 200);
    }

    /**
     * Loading undisplayed reports
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/report/load", name="ajax_load_reports")
     * @param Request $request
     * @return JsonResponse
     */
    public function loadReports(Request $request)
    {
        $reportsNumber = intval($request->get('reportsNumber'));

        $reports = $this->reportRepository->findAll();

        if (sizeof($reports) != $reportsNumber) {
            $reports = array_slice($reports, $reportsNumber);
            foreach ($reports as $report) {
                $rendered[] = $this->renderView('thread/reportListRow.html.twig', ['report' => $report]);
            }

            return new JsonResponse($rendered ?? []);
        } else {
            return new JsonResponse(null, Response::HTTP_ACCEPTED);
        }
    }

    /**
     * Loads a full version of attachment with ajax
     *
     * @Route("/attachment/{alias<[a-z]{1,4}>}/{post_id<\d+>}/{attachment_id<\d+>}", name="ajax_load_attachment")
     * @ParamConverter("post", options={"mapping": {"alias" : "board", "post_id": "inboardId"}})
     * @param Post                $post
     * @param Request             $request Request instance
     * @param SerializerInterface $serializer
     * @return JsonResponse ['attachment'=>'json object', 'post_id'=>'post id', 'attachment_id'=>'attachment id']
     */
    public function loadAttachment(Post $post, Request $request, SerializerInterface $serializer)
    {
        /* @var Attachment $attachment */
        $attachment = $post->getAttachments()[$request->attributes->get('attachment_id')];

        $json = $serializer->serialize(
            $attachment,
            'json',
            ['groups' => Attachment::LOAD]
        );

        return new JsonResponse(
            [
                'attachment' => json_decode($json, true),
                'post_id' => $post->getId(),
                'attachment_id' => $request->attributes->get('attachment_id'),
            ]
        );
    }

    /**
     * Thread deletion
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/delete/thread/{alias<[a-z]{1,4}>}/{op_post_id<\d+>}", name="ajax_thread_delete")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id" : "inboardId"}})
     * @param Post $opPost First post in thread
     * @return JsonResponse
     */
    public function deleteThread(Post $opPost)
    {
        $thread = $opPost->getThread();
        $this->getDoctrine()->getManager()->remove($thread);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('Post was successfully deleted!');
    }

    /**
     * Post deletion
     *
     * @IsGranted({"ROLE_MODER"}, message="Access denied", statusCode=403)
     * @Route("/delete/post/{alias<[a-z]{1,4}>}/{id<\d+>}", name="ajax_post_delete")
     * @ParamConverter("post", options={"mapping": {"alias" : "board", "id" : "inboardId"}})
     * @param Post $post Post instance
     * @return JsonResponse
     */
    public function deletePost(Post $post)
    {
        $this->getDoctrine()->getManager()->remove($post);
        $this->getDoctrine()->getManager()->persist($post->getThread());
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('Post was successfully deleted!');
    }

    /**
     * Loads a rendered post preview with ajax
     *
     * @Route("/preview/{alias<[a-z]{1,4}>}/{post_id<\d+>}", name="ajax_load_preview")
     * @ParamConverter("post", options={"mapping": {"alias" : "board", "post_id" : "inboardId"}})
     * @param Post    $post
     * @param Request $request Request instance
     * @return JsonResponse ['post'=>'post rendered template', 'post_id'=>'post id']
     */
    public function loadPreview(Post $post, Request $request)
    {
        $request->attributes->add(['op_post_id' => $post->getThread()->getOpPost()->getInboardId()]);
        $rendered = $this->renderView(
            'thread/post.html.twig',
            [
                'post' => $post,
                'postLoop' => null,
                'isPreview' => true,
            ]
        );

        return new JsonResponse(
            [
                'post' => $rendered,
                'post_id' => $request->attributes->get('post_id').'p',
            ]
        );
    }

    /**
     * Loads post data for adding to local storage
     *
     * @Route("/post/{alias<[a-z]{1,4}>}/{post_id<\d+>}", name="ajax_load_post")
     * @ParamConverter("post", options={"mapping": {"alias" : "board", "post_id" : "inboardId"}})
     *
     * @param Post $post
     * @return JsonResponse
     */
    public function loadPost(Post $post)
    {
        $thread = $post->getThread();

        $response = [
            'theme' => $post->getTheme(),
            'thread' => $post->getInboardId(),
            'board' => $thread->getBoard()->getAlias(),
            'lastPost' => $thread->getPosts()->last()->getInboardId(),
        ];

        return new JsonResponse($response);
    }

    /**
     * Loading posts on page when reaching bottom of it
     *
     * @Route("{alias<[a-z]{1,4}>}/res/{op_post_id<\d+>}/{offset<\d+>}/", name="ajax_load_posts")
     * @ParamConverter("opPost", options={"mapping": {"alias" : "board", "op_post_id" : "inboardId"}})
     *
     * @param Post    $opPost  First post in thread
     * @param Request $request Request instance
     * @param int     $amount  Amount of posts that need to load
     * @return JsonResponse ['posts'=>'rendered html of posts stack']
     */
    public function loadPosts(Post $opPost, Request $request, int $amount = 15)
    {
        $thread = $opPost->getThread();
        $offset = $request->attributes->get('offset');

        if($request->query->get('amount')){
           $amount = $request->query->get('amount');
        }

        $posts = $thread->getPosts($offset, $amount);

        foreach ($posts as $post) {
            $rendered[] = $this->renderView(
                'thread/post.html.twig',
                [
                    'post' => $post,
                    'postLoop' => ['index' => ++$offset],
                    'isPreview' => false,
                ]
            );
        }

        return new JsonResponse(
            [
                'posts' => $rendered ?? [],
            ]
        );
    }

    /**
     *
     * @Route("unseen_posts/{alias<[a-z]{1,4}>}/{post_id<\d+>}/", name="ajax_get_unseen_posts")
     * @ParamConverter("lastSeenPost", options={"mapping": {"alias" : "board", "post_id" : "inboardId"}})
     *
     * @param Post $lastSeenPost
     * @return JsonResponse
     */
    public function getUnseenPosts(Post $lastSeenPost)
    {
        $posts = $lastSeenPost->getThread()->getPosts();
        foreach ($posts as $post) {
            if ($post->getInboardId() == $lastSeenPost->getInboardId()) {
                $lastSeenIndex = $lastSeenPost->getThread()->getPosts()->indexOf($post);
                $unSeenPosts = sizeof(
                    $lastSeenPost->getThread()->getPosts($lastSeenIndex + 1, $posts->count() - $lastSeenIndex)
                );

                return new JsonResponse(json_encode($unSeenPosts));
            }
        }

        return new JsonResponse(json_encode(0));
    }
}