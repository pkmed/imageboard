<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Entity\PostReport;
use App\Repository\PostReportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OverviewController
 *
 * @Route("/admin/reports")
 *
 * @package App\Controller\Admin
 */
class ReportsController extends AbstractController
{
    /**
     * @var PostReportRepository
     */
    public $reportRepository;

    public function __construct(PostReportRepository $reportRepository)
    {
        $this->reportRepository = $reportRepository;
    }

    /**
     * @Route("/", name="admin_reports_index")
     */
    public function index()
    {
        $reports = $this->reportRepository->findAll();

        return $this->render(
            'adminPanel/reports/reports.html.twig',
            [
                'reports' => $reports,
            ]
        );
    }

    /**
     * @Route("/delete/report/{id}", name="admin_reports_delete_report")
     * @param PostReport $report
     * @return RedirectResponse
     */
    public function deleteReport(PostReport $report)
    {
        $this->getDoctrine()->getManager()->remove($report);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->generateUrl('admin_reports_index'));
    }

    /**
     * @Route("/delete/post/{id}", name="admin_reports_delete_post")
     * @param Post $post
     * @return RedirectResponse
     */
    public function deletePost(Post $post)
    {
        $this->getDoctrine()->getManager()->remove($post);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->generateUrl('admin_reports_index'));
    }
}