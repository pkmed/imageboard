<?php

namespace App\Controller\Admin;

use App\Entity\BoardTheme;
use App\Form\BoardThemeType;
use App\Repository\BoardThemeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OverviewController
 *
 * @Route("/admin/generals")
 *
 * @package App\Controller\Admin
 */
class BoardThemesController extends AbstractController
{
    /**
     * @var BoardThemeRepository
     */
    public $boardThemeRepository;

    /**
     * BoardThemesController constructor.
     *
     * @param BoardThemeRepository $boardThemeRepository
     */
    public function __construct(BoardThemeRepository $boardThemeRepository)
    {
        $this->boardThemeRepository = $boardThemeRepository;
    }

    /**
     * @Route("/", name="admin_generals_index")
     * @return Response
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $themes = $this->boardThemeRepository->findAll();

        return $this->render(
            'adminPanel/generalThemes/generalsTable.html.twig',
            [
                'boardThemes' => $themes,
            ]
        );
    }

    /**
     * @Route("/create", name="admin_generals_create")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $form = $this->createForm(BoardThemeType::class, new BoardTheme());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $boardTheme = $form->getData();
            $this->getDoctrine()->getManager()->persist($boardTheme);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('admin_generals_index'));
        }

        return $this->render(
            'adminPanel/form.html.twig',
            [
                'form' => $form->createView(),
                'formHeader' => 'New theme',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="admin_generals_edit")
     * @param BoardTheme $boardTheme
     * @param Request    $request
     * @return RedirectResponse|Response
     */
    public function edit(BoardTheme $boardTheme, Request $request)
    {
        $form = $this->createForm(
            BoardThemeType::class,
            $boardTheme,
            ['attr' => ['boardThemeId' => $boardTheme->getId()]]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $boardTheme = $form->getData();
            $this->getDoctrine()->getManager()->persist($boardTheme);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('admin_generals_index'));
        }

        return $this->render(
            'adminPanel/form.html.twig',
            [
                'form' => $form->createView(),
                'formHeader' => 'Edit theme',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_generals_delete")
     * @param BoardTheme $boardTheme
     * @return RedirectResponse
     */
    public function delete(BoardTheme $boardTheme)
    {
        $this->getDoctrine()->getManager()->remove($boardTheme);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->generateUrl('admin_generals_index'));
    }
}