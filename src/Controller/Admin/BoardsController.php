<?php

namespace App\Controller\Admin;

use App\Entity\Board;
use App\Form\BoardType;
use App\Repository\BoardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BoardsController
 *
 * @Route("/admin/boards")
 *
 * @package App\Controller\Admin
 */
class BoardsController extends AbstractController
{
    /**
     * @var BoardRepository
     */
    private $boardRepository;

    /**
     * BoardsController constructor.
     *
     * @param BoardRepository $boardRepository
     */
    public function __construct(BoardRepository $boardRepository)
    {
        $this->boardRepository = $boardRepository;
    }

    /**
     * @Route("/", name="admin_boards_index")
     * @return Response
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $boards = $this->boardRepository->findAll();

        return $this->render(
            'adminPanel/boards/boards.html.twig',
            [
                'boards' => $boards,
            ]
        );
    }

    /**
     * @Route("/create", name="admin_boards_create")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $form = $this->createForm(BoardType::class, new Board());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $board = $form->getData();
            $this->getDoctrine()->getManager()->persist($board);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('admin_boards_index'));
        }

        return $this->render(
            'adminPanel/form.html.twig',
            [
                'form' => $form->createView(),
                'formHeader' => 'New board',
            ]
        );
    }

    /**
     * @Route("/edit/{alias}", name="admin_boards_edit")
     * @param Board   $board
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Board $board, Request $request)
    {
        $form = $this->createForm(BoardType::class, $board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $board = $form->getData();
            $this->getDoctrine()->getManager()->persist($board);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('admin_boards_index'));
        }

        return $this->render(
            'adminPanel/form.html.twig',
            [
                'form' => $form->createView(),
                'formHeader' => 'Edit board',
            ]
        );
    }

    /**
     * @Route("/delete/{alias}", name="admin_boards_delete")
     * @param Board $board
     * @return RedirectResponse
     */
    public function delete(Board $board)
    {
        $this->getDoctrine()->getManager()->remove($board);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->generateUrl('admin_boards_index'));
    }
}