<?php

namespace App\Controller\Admin;

use App\Form\SiteCommonsType;
use App\Repository\SiteCommonsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OverviewController
 *
 * @Route("/admin/overview")
 *
 * @package App\Controller\Admin
 */
class OverviewController extends AbstractController
{
    /**
     * @var SiteCommonsRepository
     */
    public $siteCommonsRepository;

    public function __construct(SiteCommonsRepository $siteCommonsRepository)
    {
        $this->siteCommonsRepository = $siteCommonsRepository;
    }

    /**
     * @Route("/", name="admin_overview_index")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function index(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $commons = $this->siteCommonsRepository->findAll()[0];

        $form = $this->createForm(SiteCommonsType::class, $commons);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commons = $form->getData();
            if ($newLogo = $form->get('logo')->getData()) {
                /** @var UploadedFile $newLogo */
                $dest = $this->getParameter('app.images_dir');
                $newLogo->move($dest.'/', 'logo.'.$newLogo->getClientOriginalExtension());
            }

            $this->getDoctrine()->getManager()->persist($commons);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notification', 'Edited successfully');

            return new RedirectResponse($this->generateUrl('admin_overview_index'));
        }

        return $this->render(
            'adminPanel/overview/overview.html.twig',
            [
                'commons' => $commons,
                'formHeader' => 'Overview',
                'form' => $form->createView(),
            ]
        );
    }

}