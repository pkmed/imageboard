<?php

namespace App\CLI;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateAdminProfile extends CreateProfile
{
    protected static $defaultName = 'app:create:admin';

    /**
     * CreateAdminProfile constructor.
     *
     * @param UserRepository               $userRepository
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        parent::__construct($userRepository, $entityManager, $passwordEncoder, User::ROLE_ADMIN);
    }

    protected function configure()
    {
        $this->setDescription("Creates admin profile in db");
    }

    /**
     * Creates account for administration
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);
    }
}