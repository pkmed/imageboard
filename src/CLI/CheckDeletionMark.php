<?php

namespace App\CLI;

use App\Repository\ThreadRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckDeletionMark extends Command
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var ThreadRepository
     */
    public $threadRepository;
    /**
     * @var EntityManagerInterface
     */
    public $entityManager;

    /**
     * CheckDeletionMark constructor.
     *
     * @param EntityManagerInterface $entityManager    An Entity Manager Interface instance.
     * @param ThreadRepository       $threadRepository Thread repository instance.
     * @param string|null            $name             Inherited parameter from
     *                                                 Symfony\Component\Console\Command\Command.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ThreadRepository $threadRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->name = $name;
        $this->threadRepository = $threadRepository;
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'app:thread:check:deletion';

    protected function configure()
    {
        $this->setDescription("Checks deletion mark of threads and remove thread ");
    }

    /**
     * Deletes threads tagged with MTD
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $threads = $this->threadRepository->findAll();
        $deletedNumber = 0;
        foreach ($threads as $thread) {
            if ($thread->isMarkedToDelete()) {
                $this->entityManager->remove($thread);
                $deletedNumber++;
            }
        }
        $this->entityManager->flush();
        $output->writeln('Threads deleted: '.$deletedNumber);
    }
}