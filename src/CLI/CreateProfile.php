<?php

namespace App\CLI;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class CreateProfile extends Command
{
    /**
     * @var string
     */
    public $role;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        string $role,
        string $name = null
    ) {
        parent::__construct($name);
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->role = $role;
    }

    /**
     * Creates account for administration
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $adminEmail = new Question('Enter your e-mail: '.PHP_EOL, 'your@mail.com');
        $helper = $this->getHelper('question');
        $adminEmail->setValidator(
            function ($value) {
                if (!preg_match('~^(\w|-|\+){4,256}@\w{4,256}\.\w{2,256}$~', $value)) {
                    throw new Exception('Non-valid e-mail');
                }
                if ($this->userRepository->findBy(['email' => $value])) {
                    throw new Exception('Email already exists');
                }

                return $value;
            }
        );

        $email = $helper->ask($input, $output, $adminEmail);

        $adminPassword = new Question('Enter your password: '.PHP_EOL);
        $adminPassword->setHidden(true);
        $adminPassword->setHiddenFallback(false);
        $helper = $this->getHelper('question');
        $adminPassword->setValidator(
            function ($value) {
                if (trim($value) == '') {
                    throw new Exception('The password cannot be empty');
                }
                if (!preg_match('~^(?=.*?[0-9])(?=.*?[a-zA-Z])[0-9A-Za-z]{8,16}$~', $value)) {
                    throw new Exception('The password must contain combination of letters and numbers and be from 8 to 16 symbols');
                }

                return $value;
            }
        );

        $password = $helper->ask($input, $output, $adminPassword);

        $admin = new User();
        $admin->setEmail($email);
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, $password));
        $admin->setRoles([$this->role]);

        $this->entityManager->persist($admin);
        $this->entityManager->flush();

        $output->writeln(ucfirst(strtolower(explode('_', $this->role)[1])).' profile was created successfully!');
    }
}