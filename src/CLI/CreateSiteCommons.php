<?php

namespace App\CLI;

use App\Entity\SiteCommons;
use App\Repository\SiteCommonsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateSiteCommons extends Command
{
    protected static $defaultName = 'app:create:siteCommons';
    /**
     * @var EntityManagerInterface
     */
    public $entityManager;
    /**
     * @var ValidatorInterface
     */
    public $validator;
    /**
     * @var ContainerInterface
     */
    public $container;
    /**
     * @var SiteCommonsRepository
     */
    public $siteCommonsRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        ContainerInterface $container,
        SiteCommonsRepository $siteCommonsRepository,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->container = $container;
        $this->siteCommonsRepository = $siteCommonsRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commons = new SiteCommons();
        $helper = $this->getHelper('question');

        $siteNameQuestion = new Question('Enter site name:' . PHP_EOL);
        $commons->setSiteName($helper->ask($input, $output, $siteNameQuestion));

        $logoQuestion = new Question('Enter path to logo(can be empty):' . PHP_EOL);
        $logo = $helper->ask($input, $output, $logoQuestion);
        if ($logo != null) {
            $file = new File($logo);
            //validate image
            $violations = $this->validator->validatePropertyValue(SiteCommons::class, 'logo', $file);
            if (sizeof($violations) == 0) {
                $dest = $this->container->getParameter('app.images_dir') . '/' . $file->getFilename();
                if (copy($logo, $dest)) {
                    $newName = $this->container->getParameter('app.images_dir') . '/logo.' . $file->getExtension();
                    rename($dest, $newName);
                    $commons->setLogo('uploads/images/logo.' . $file->getExtension());
                }
            }
        }

        $disclaimerQuestion = new Question('Type disclaimer for main page(can be empty):' . PHP_EOL);
        $commons->setDisclaimer($helper->ask($input, $output, $disclaimerQuestion) ?? '');

        $commons->setTotalPosts(0);
        $commons->setTotalThreads(0);

        $copyrightQuestion = new Question('Enter copyright(can be empty):' . PHP_EOL);
        $commons->setCopyright($helper->ask($input, $output, $copyrightQuestion) ?? '');

        if ($oldCommons = $this->siteCommonsRepository->findAll()) {
            $this->entityManager->remove($oldCommons[0]);
        }
        $this->entityManager->persist($commons);
        $this->entityManager->flush();
    }

    protected function configure()
    {
        $this->setDescription("Creates SiteCommons instance in db");
    }

}