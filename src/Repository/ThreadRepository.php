<?php

namespace App\Repository;

use App\Entity\Thread;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;

/**
 * @method Thread|null find($id, $lockMode = null, $lockVersion = null)
 * @method Thread|null findOneBy(array $criteria, array $orderBy = null)
 * @method Thread[]    findAll()
 * @method Thread[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThreadRepository extends ServiceEntityRepository
{
    /**
     * ThreadRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Thread::class);
    }

    /**
     * Gets first 20 threads sorted by last_update
     *
     * @param $boardAlias string Alias of the board that threads belong to
     * @return ArrayCollection Threads collection
     */
    public function find20RecentUpdated($boardAlias)
    {
        return $this->createQueryBuilder('t')
            ->where('t.board=:board')
            ->orderBy('t.pin', 'DESC')
            ->addOrderBy('t.lastUpdate', 'DESC')
            ->setMaxResults(20)
            ->setParameter('board', $boardAlias)
            ->getQuery()
            ->getResult();
    }

    /**
     * Removes mark to deletion from 20 recently updated threads,
     * sets it to rest of threads.
     * Sets MTD to all threads that reached their bump limit
     *
     * @param $boardAlias
     * @throws DBALException
     */
    public function updateMarksToDeletion($boardAlias)
    {
        $this->getEntityManager()->getConnection()->executeUpdate(
            '
            update imageboard.thread set deletion_mark=0
            where imageboard.thread.id in (
                select * 
                from (
                    select imageboard.thread.id 
                    from imageboard.thread 
                    where board_alias=:alias 
                    ORDER by last_update DESC 
                    limit 20
                ) as t
            );
            
            update imageboard.thread set deletion_mark=1 
            where imageboard.thread.id in (
                select * 
                from (
                    select imageboard.thread.id 
                    from imageboard.thread 
                    where board_alias=:alias 
                    ORDER by last_update DESC 
                    limit 20, 100
                ) as t
            ) OR imageboard.thread.post_number >= imageboard.thread.bump_limit;
        ',
            ['alias' => $boardAlias]
        );
    }
}
