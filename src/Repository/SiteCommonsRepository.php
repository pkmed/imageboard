<?php

namespace App\Repository;

use App\Entity\SiteCommons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SiteCommons|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteCommons|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteCommons[]    findAll()
 * @method SiteCommons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteCommonsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteCommons::class);
    }

    // /**
    //  * @return SiteCommons[] Returns an array of SiteCommons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteCommons
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
