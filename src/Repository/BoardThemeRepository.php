<?php

namespace App\Repository;

use App\Entity\BoardTheme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method BoardTheme|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoardTheme|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoardTheme[]    findAll()
 * @method BoardTheme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardThemeRepository extends ServiceEntityRepository
{
    /**
     * BoardThemeRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoardTheme::class);
    }

    /**
     * @return mixed
     */
    public function getNotEmptyThemes()
    {
        return $this->createQueryBuilder('th')
            ->innerJoin('th.boards', 'b', Join::WITH, 'b.boardTheme = th')
            ->getQuery()->getResult();

    }
}
